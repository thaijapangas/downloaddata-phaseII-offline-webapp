﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mamgements.aspx.cs" Inherits="UI.Admin.Mamgements" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Thai Japan Gas</title>
    <link rel="stylesheet" href="_css/popup.css" />
    <link rel="stylesheet" href="_css/btnbootstrap.css" />
    <link rel="stylesheet" href="_css/button.css" />
    <link rel="stylesheet" type="text/css" href="../_css/main.css" />
    <link rel="stylesheet" type="text/css" href="../_css/styles.css" />
    <link rel="stylesheet" href="_css/element-view.css" />
    <script src="../_js/script.js" type="text/javascript"></script>
    <script src="_js/jquery1.7.1.js" type="text/javascript"></script>
    <script src="_js/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="_js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("[id*=dgvCustomer] td").hover(function () {
                $("td", $(this).closest("tr")).addClass("hover_row");
            }
            ,
            function () {
                $("td", $(this).closest("tr")).removeClass("hover_row");
            });

            $("[id*=dgvItems] td").hover(function () {
                $("td", $(this).closest("tr")).addClass("hover_row");
            }
            ,
            function () {
                $("td", $(this).closest("tr")).removeClass("hover_row");
            });
        });

        function selectText() {
            document.getElementById("<%=txtpageIndex.ClientID %>").select();
            document.getElementById("<%=txtpageIndex.ClientID %>").focus();
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function DisplayLoading() {
            if ($('#upFile').val().length) {
                document.getElementById("loading").style.display = 'block';
            }
            else {
                document.getElementById("loading").style.display = 'none';
            }
        }

        function Confirm() {
            if (confirm("คุณต้องการรับข้อมูลจาก Hand Held  ใช่หรือไม่?")) {
                document.getElementById('txthidConfirm').value = "Yes";
            } else {
                document.getElementById('txthidConfirm').value = "No";
            }
        }
    </script>

    <style type="text/css">
        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../_images/InitialImage.png") no-repeat right top;
            /*background-color:#089bfc;*/
            color: Black;
            font-weight: bold;
        }

            .Initial:hover {
                color: White;
                background: url("../_images/SelectedButton.png") no-repeat right top;
                /*background-color:#006699;*/
            }

        .Clicked {
            float: left;
            display: block;
            background: url("../_images/SelectedButton.png") no-repeat right top;
            /*background-color:#006699;*/
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }

        .auto-style1 {
            width: 128px;
        }

        .auto-style4 {
            width: 208px;
        }

        .auto-style5 {
            width: 80px;
        }

        #loading-image {
            border-width: 0px;
            position: fixed;
            padding: 50px;
            left: 40%;
            top: 30%;
        }
    </style>
</head>
<body style="font-family: 'Open Sans', sans-serif;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server" />
        <asp:Timer runat="server" id="UpdateTimer" interval="1000" ontick="UpdateTimer_Tick" />
        <div id="loading" runat="server" style="position: fixed; text-align: center; display: none; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <img id="loading-image" src="_images/loading.gif" />
        </div>
        <asp:UpdatePanel ID="upnmain" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger controlid="UpdateTimer" eventname="Tick" />
            </Triggers>
            <ContentTemplate>
                <div class="header">
                    <div class="fullpage">
                        <div id='cssmenu'>
                            <table>
                                <tr>
                                    <td style="height: 20px;"></td>
                                </tr>
                            </table>
                            <div class="login" id="bt_login">
                                <a href="Login.aspx" style="text-decoration: none; color: black;">
                                    <asp:Label ID="lblLogout" runat="server" Text="Log out" ForeColor="White" Font-Bold="true" Width="55px"></asp:Label></a>
                            </div>
                        </div>
                    </div>
                </div>
                <table style="width: 98%; margin-left: auto; margin-right: auto;" cellpadding="1"
                    cellspacing="1">
                    <tr>
                        <td style="height: 45px;">
                            <a href="Mamgements.aspx">
                                <asp:Image ID="imgMain" runat="server" ImageUrl="~/_images/LOGOTJG2.png" ImageAlign="Left" Width="220px" Height="80px" ToolTip="Thai Japan Gas Co.,Ltd" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 30px; text-align:right;">
                            <asp:Label ID="lblVersion" runat="server" Text="Version : x.x.x.x" Font-Bold="True" Font-Size="Medium"></asp:Label>
                            <asp:HiddenField ID="txthidConfirm" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%--button button-action -> สีเขียว--%>
                            <%--button button-highlight -> สีส้ม--%>
                            <%--button button-caution -> สีแดง--%>
                            <%--button button-royal -> สีม่วง--%>
                            <asp:Button ID="btnReceiveData" runat="server" CssClass="button button-primary" Text="Receive Data" OnClick="btnReceiveData_Click" OnClientClick = "Confirm();" />&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 20px;"></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnperson" runat="server" GroupingText="Transaction List" Width="98%" Style="text-align: left;">
                                <asp:Button Text="1.1.2 GI Ref Delivery" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                                    OnClick="Tab1_Click" />
                                <asp:Button Text="1.2 Distribution Ret" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                                    OnClick="Tab2_Click" />
                                <asp:Button Text="1.4.1 GRProduction" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                                    OnClick="Tab3_Click" />
                                <asp:Button Text="  1.5.3 GI Ref Empty" BorderStyle="None" ID="Tab4" CssClass="Initial" runat="server"
                                    OnClick="Tab4_Click" />
                                <asp:MultiView ID="MainView" runat="server">
                                    <asp:View ID="View1" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvGIRefDelivery" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="do_no" HeaderText="Invoice / DO">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="serial_number" HeaderText="S/N">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View2" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvDistributionRet" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="empty_or_full" HeaderText="Empty / Full">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="90px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="cust_id" HeaderText="Custoemr Code">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="doc_no" HeaderText="Document Number">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="150px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="serial_number" HeaderText="S/N">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="vehicle" HeaderText="Vehicle">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View3" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvGRFromProduction" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="pre_order" HeaderText="Pre Order">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="batch" HeaderText="Batch">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="110px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="serial_number" HeaderText="S/N">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                    <asp:View ID="View4" runat="server">
                                        <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="dgvGIRefIOEmpty" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <HeaderStyle BackColor="#006699" Height="30px"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundField DataField="create_date" HeaderText="Create Date">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="do_no" HeaderText="Invoice / DO">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="serial_number" HeaderText="S/N">
                                                                <HeaderStyle HorizontalAlign="Center" ForeColor="White" />
                                                                <ItemStyle Width="120px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </asp:MultiView>
                                <br />
                                <asp:Panel ID="NavigationPanel" Visible="true" runat="server">
                                    <table border="0" cellpadding="3" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSearchingCount" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100">Page
                                            <asp:Label ID="CurrentPageLabel" runat="server" />
                                                of
                                            <asp:Label ID="TotalPagesLabel" runat="server" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Firstpage" Text="<< Start" runat="server" Style="text-decoration: none; color: #089bfc;" OnClick="Firstpage_Click" />
                                            </td>
                                            <td style="width: 60px">
                                                <asp:LinkButton ID="PreviousButton" Text="< Prev" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="PreviousButton_Click" />
                                            </td>
                                            <td style="width: 60px">
                                                <asp:LinkButton ID="NextButton" Text="Next >" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="NextButton_Click" />
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="Endpage" Text="End >>" Style="text-decoration: none; color: #089bfc;" runat="server" OnClick="Endpage_Click" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblpageIndex" runat="server" Text="Page"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtpageIndex" runat="server" Width="35px" MaxLength="4" Height="17px" onkeypress="return isNumberKey(event);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnGO" runat="server" Text="Go" Width="40px" Style="text-align: center;" CssClass="btnOpen btnOpen-5" Height="22px" OnClick="btnGO_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 15px;"></td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Button ID="btnActivateLicence" runat="server" CssClass="btn btn-danger" Text="Activate Licences" OnClick="btnActivateLicence_Click" Width="153px" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnExport" runat="server" CssClass="btn btn-success" Text="Export To Excel File" Visible="true" OnClick="btnExport_Click" Width="154px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>

                    <tr>
                        <td style="height: 15px; text-align:left;">
                            <asp:Label ID="label1" runat="server" Text="(PDA) Sync Computer :" Font-Bold="True" Font-Size="Larger"></asp:Label>&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblResultSync" runat="server" Text="Sync HandHeld" Font-Bold="True" Font-Size="Larger"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
