﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlServerCe;
using System.Globalization;
using Utilitys.Events;

namespace UI.Admin.Execute
{
    public class Executing : UI.Admin.Connections.Connection, IDisposable
    {
        #region Instance

        private static Executing _Instance = new Executing();
        public static Executing Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Executing();
                }
                return _Instance;
            }
        }
        #endregion

        #region Prperty

        string connectionString = @"Data Source=C:\Data Base SDF\TJG.sdf;Password=pass;Persist Security Info=True";
        private SqlCeEngine sqlCEEngine;

        #endregion

        #region 0. getDateTime

        public DateTime GetDateServer()
        {
            try
            {
                DateTime dtpDateTime = DateTime.Now;

                string sql = "SELECT GETDATE() AS getDateTime";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                DataTable dtDateTime = (DataTable)dbManager.ExecuteToDataTable(command);

                return (DateTime)dtDateTime.Rows[0]["getDateTime"];
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region 1. [1.1.2] Tab GIDelivery

        public System.Data.DataTable getGIDelivery()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, do_no, serial_number FROM GI_Delivery";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 2. [1.2] Tab DistributionRet

        public System.Data.DataTable getDistributionRet()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, empty_or_full, cust_id, doc_no, serial_number, vehicle FROM Distribution_Ret";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                //this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 3. [1.4.1] Tab GIRefIOEmpty

        public System.Data.DataTable getGIRefIOEmpty()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, do_no, serial_number FROM GI_Ref_IO_Empty";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 4. [1.5.3] Tab GRFromProduction

        public System.Data.DataTable getGRFromProduction()
        {
            System.Data.DataTable result;
            try
            {
                System.Data.DataTable dataTable = new System.Data.DataTable();
                string text = "SELECT create_date, pre_order, batch, serial_number FROM GR_Production";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = System.Data.CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
                this.dbManager.Close();
                result = dataTable;
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        #endregion

        #region 5. Insert Log

        public bool Insert_Log(string error_code, string error_message, string page_form, string method_name)
        {
            bool _result = false;
            try
            {
                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
                sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message, page_form, method_name));

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();

                dbManager.Open();
                if (dbManager.ExecuteNonQuery(command) > 0)
                    _result = true;
            }
            catch (SqlCeException)
            {
                _result = false;
            }
            finally
            {
                if (dbManager != null)
                {
                    dbManager.Dispose();
                }
            }
            return _result;
        }

        #endregion

        #region 6. Activate Licence

        public string CheckExpireSoftware()
        {
            string connti = "FALSE";
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

                DataTable dt_licence = new DataTable();

                string sql = "SELECT TOP(1) expire_date " +
                             "FROM t_setup ";

                SqlCeCommand command = new SqlCeCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = sql.ToString();
                dbManager.Open();
                dt_licence = dbManager.ExecuteToDataTable(command);

                if (!dt_licence.IsNullOrNoRows())
                {
                    #region Has data

                    string _expire_date = dt_licence.Rows[0][0].ToString().Trim();
                    if (string.IsNullOrEmpty(_expire_date))
                    {
                        connti = "FALSE";
                    }
                    else if (!string.IsNullOrEmpty(_expire_date))
                    {
                        string _decry_expire_date = Convert.ToString(Convert.ToInt64(_expire_date) - 39335472);
                        if (_decry_expire_date.Trim() == "30000101")
                        {
                            connti = "LIFETIME";
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_decry_expire_date))
                            {
                                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

                                if (Convert.ToInt64(_decry_expire_date) < Convert.ToInt64(_crrent_date))
                                    connti = "FALSE";
                                else if (Convert.ToInt64(_decry_expire_date) > Convert.ToInt64(_crrent_date))
                                    connti = "TRUE";
                                else
                                    connti = "FALSE";
                            }
                            else
                            {
                                connti = "FALSE";
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception)
            {
                connti = "FALSE";
            }

            return connti;
        }
        public bool ActivateLicence(string _licence_key)
        {
            bool result = false;
            try
            {
                string text = "UPDATE t_setup SET expire_date = '" + _licence_key.Trim() + "', update_date = GETDATE() ";
                SqlCeCommand sqlCeCommand = new SqlCeCommand();
                sqlCeCommand.CommandType = CommandType.Text;
                sqlCeCommand.CommandText = text.ToString();
                this.dbManager.Open();
                this.dbManager.ExecuteNonQuery(sqlCeCommand);
                this.dbManager.Close();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
        public string ValidationLicence(string _licence_keys)
        {
            string Result = string.Empty;
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                string _licence_key = Convert.ToString((Convert.ToInt64(_licence_keys) - 39335472));
                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);
                if (!string.IsNullOrEmpty(_licence_key))
                {
                    if (_licence_key.Trim() == "30000101")
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            Result = "TRUE";
                        }
                    }
                    else if (_licence_key.IndexOf("2016") >= 0)
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            DateTime dTime = DateTime.ParseExact(_licence_key, "yyyyMMdd", null);
                            string display_date = dTime.ToLongDateString();
                            Result = "TRUE_EXPRIE|" + display_date;
                        }
                    }
                    else if (Convert.ToInt32(_licence_key) < Convert.ToInt32(_crrent_date))
                    {
                        Result = "TRUE_EXPIRED";
                    }
                    else
                    {
                        Result = "FALSE";
                    }
                }
            }
            catch (Exception)
            {
                Result = "FALSE";
            }

            return Result;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }

    //public class Executing : IDisposable//UI.Admin.Connections.Connection, IDisposable
    //{
    //    #region Instance

    //    private static Executing _Instance = new Executing();
    //    public static Executing Instance
    //    {
    //        get
    //        {
    //            if (_Instance == null)
    //            {
    //                _Instance = new Executing();
    //            }
    //            return _Instance;
    //        }
    //    }
    //    #endregion

    //    #region Prperty

    //    string connectionString = @"Data Source=C:\Data Base SDF\TJG.sdf;Password=pass;Persist Security Info=True";
    //    private SqlCeEngine sqlCEEngine;

    //    #endregion

    //    #region 0. getDateTime

    //    public DateTime GetDateServer()
    //    {
    //        try
    //        {
    //            DateTime dtpDateTime = DateTime.Now;
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = "SELECT GETDATE() AS getDateTime";
    //            command.CommandType = System.Data.CommandType.Text;
    //            SqlCeDataReader dr = command.ExecuteReader();
    //            dataTable.Load(dr);
    //            DataTable dtDateTime = dataTable;

    //            return (DateTime)dtDateTime.Rows[0]["getDateTime"];

    //            /*
    //            DateTime dtpDateTime = DateTime.Now;

    //            string sql = "SELECT GETDATE() AS getDateTime";

    //            SqlCeCommand command = new SqlCeCommand();
    //            command.CommandType = CommandType.Text;
    //            command.CommandText = sql.ToString();
    //            dbManager.Open();
    //            DataTable dtDateTime = (DataTable)dbManager.ExecuteToDataTable(command);

    //            return (DateTime)dtDateTime.Rows[0]["getDateTime"];
    //            */
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //    }

    //    #endregion

    //    #region 1. [1.1.2] Tab GIDelivery

    //    public System.Data.DataTable getGIDelivery()
    //    {
    //        System.Data.DataTable result;
    //        try
    //        {
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            sqlCEEngine.Dispose();
    //            conn.Open();

    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = "SELECT create_date, do_no, serial_number FROM GI_Delivery";
    //            command.CommandType = System.Data.CommandType.Text;
    //            SqlCeDataReader dr = command.ExecuteReader();
    //            dataTable.Load(dr);
    //            result = dataTable;
    //            conn.Close();

    //            /*
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            string text = "SELECT create_date, do_no, serial_number FROM GI_Delivery";
    //            SqlCeCommand sqlCeCommand = new SqlCeCommand();
    //            sqlCeCommand.CommandType = System.Data.CommandType.Text;
    //            sqlCeCommand.CommandText = text.ToString();
    //            this.dbManager.Open();
    //            dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
    //            this.dbManager.Close();
    //            result = dataTable;
    //            */
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return result;
    //    }

    //    #endregion

    //    #region 2. [1.2] Tab DistributionRet

    //    public System.Data.DataTable getDistributionRet()
    //    {
    //        System.Data.DataTable result;
    //        try
    //        {
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = "SELECT create_date, empty_or_full, cust_id, doc_no, serial_number, vehicle FROM Distribution_Ret";
    //            command.CommandType = System.Data.CommandType.Text;
    //            SqlCeDataReader dr = command.ExecuteReader();
    //            dataTable.Load(dr);
    //            result = dataTable;

    //            /*
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            string text = "SELECT create_date, empty_or_full, cust_id, doc_no, serial_number, vehicle FROM Distribution_Ret";
    //            SqlCeCommand sqlCeCommand = new SqlCeCommand();
    //            sqlCeCommand.CommandType = System.Data.CommandType.Text;
    //            sqlCeCommand.CommandText = text.ToString();
    //            this.dbManager.Open();
    //            dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
    //            //this.dbManager.Close();
    //            result = dataTable;
    //            */
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return result;
    //    }

    //    #endregion

    //    #region 3. [1.4.1] Tab GIRefIOEmpty

    //    public System.Data.DataTable getGIRefIOEmpty()
    //    {
    //        System.Data.DataTable result;
    //        try
    //        {
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = "SELECT create_date, do_no, serial_number FROM GI_Ref_IO_Empty";
    //            command.CommandType = System.Data.CommandType.Text;
    //            SqlCeDataReader dr = command.ExecuteReader();
    //            dataTable.Load(dr);
    //            result = dataTable;

    //            /*
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            string text = "SELECT create_date, do_no, serial_number FROM GI_Ref_IO_Empty";
    //            SqlCeCommand sqlCeCommand = new SqlCeCommand();
    //            sqlCeCommand.CommandType = System.Data.CommandType.Text;
    //            sqlCeCommand.CommandText = text.ToString();
    //            this.dbManager.Open();
    //            dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
    //            this.dbManager.Close();
    //            result = dataTable;
    //            */
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return result;
    //    }

    //    #endregion

    //    #region 4. [1.5.3] Tab GRFromProduction

    //    public System.Data.DataTable getGRFromProduction()
    //    {
    //        System.Data.DataTable result;
    //        try
    //        {
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = "SELECT create_date, pre_order, batch, serial_number FROM GR_Production";
    //            command.CommandType = System.Data.CommandType.Text;
    //            SqlCeDataReader dr = command.ExecuteReader();
    //            dataTable.Load(dr);
    //            result = dataTable;

    //            /*
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            string text = "SELECT create_date, pre_order, batch, serial_number FROM GR_Production";
    //            SqlCeCommand sqlCeCommand = new SqlCeCommand();
    //            sqlCeCommand.CommandType = System.Data.CommandType.Text;
    //            sqlCeCommand.CommandText = text.ToString();
    //            this.dbManager.Open();
    //            dataTable = this.dbManager.ExecuteToDataTable(sqlCeCommand);
    //            this.dbManager.Close();
    //            result = dataTable;
    //            */
    //        }
    //        catch (Exception)
    //        {
    //            throw;
    //        }
    //        return result;
    //    }

    //    #endregion

    //    #region 5. Insert Log

    //    public bool Insert_Log(string error_code, string error_message, string page_form, string method_name)
    //    {
    //        bool _result = false;
    //        try
    //        {
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            StringBuilder sql = new StringBuilder();
    //            sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
    //            sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message, page_form, method_name));
    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = sql.ToString();
    //            command.CommandType = System.Data.CommandType.Text;
    //            if (command.ExecuteNonQuery() > 0)
    //                _result = true;

    //            /*
    //            StringBuilder sql = new StringBuilder();
    //            sql.Append("INSERT INTO Action_Log (id, error_code, error_message, page_form, method_name, create_date)  ");
    //            sql.Append(string.Format("VALUES ('{0}','{1}','{2}', '{3}', '{4}', getdate()) ", Guid.NewGuid().ToString().ToUpper().Trim(), error_code, error_message, page_form, method_name));

    //            SqlCeCommand command = new SqlCeCommand();
    //            command.CommandType = CommandType.Text;
    //            command.CommandText = sql.ToString();

    //            dbManager.Open();
    //            if (dbManager.ExecuteNonQuery(command) > 0)
    //                _result = true;
    //            */
    //        }
    //        catch (SqlCeException)
    //        {
    //            _result = false;
    //        }
    //        finally
    //        {
    //            /*
    //            if (dbManager != null)
    //            {
    //                dbManager.Dispose();
    //            }
    //            */
    //        }
    //        return _result;
    //    }

    //    #endregion

    //    #region 6. Activate Licence

    //    public string CheckExpireSoftware()
    //    {
    //        string connti = "FALSE";
    //        try
    //        {
    //            DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;

    //            System.Data.DataTable dt_licence = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = "SELECT TOP(1) expire_date FROM t_setup";
    //            command.CommandType = System.Data.CommandType.Text;
    //            SqlCeDataReader dr = command.ExecuteReader();
    //            dt_licence.Load(dr);

    //            /*
    //            DataTable dt_licence = new DataTable();

    //            string sql = "SELECT TOP(1) expire_date " +
    //                         "FROM t_setup ";

    //            SqlCeCommand command = new SqlCeCommand();
    //            command.CommandType = CommandType.Text;
    //            command.CommandText = sql.ToString();
    //            dbManager.Open();
    //            dt_licence = dbManager.ExecuteToDataTable(command);
    //            */

    //            if (!dt_licence.IsNullOrNoRows())
    //            {
    //                #region Has data

    //                string _expire_date = dt_licence.Rows[0][0].ToString().Trim();
    //                if (string.IsNullOrEmpty(_expire_date))
    //                {
    //                    connti = "FALSE";
    //                }
    //                else if (!string.IsNullOrEmpty(_expire_date))
    //                {
    //                    string _decry_expire_date = Convert.ToString(Convert.ToInt64(_expire_date) - 39335472);
    //                    if (_decry_expire_date.Trim() == "30000101")
    //                    {
    //                        connti = "LIFETIME";
    //                    }
    //                    else
    //                    {
    //                        if (!string.IsNullOrEmpty(_decry_expire_date))
    //                        {
    //                            string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

    //                            if (Convert.ToInt64(_decry_expire_date) < Convert.ToInt64(_crrent_date))
    //                                connti = "FALSE";
    //                            else if (Convert.ToInt64(_decry_expire_date) > Convert.ToInt64(_crrent_date))
    //                                connti = "TRUE";
    //                            else
    //                                connti = "FALSE";
    //                        }
    //                        else
    //                        {
    //                            connti = "FALSE";
    //                        }
    //                    }
    //                }

    //                #endregion
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            connti = "FALSE";
    //        }

    //        return connti;
    //    }
    //    public bool ActivateLicence(string _licence_key)
    //    {
    //        bool result = false;
    //        try
    //        {
    //            System.Data.DataTable dataTable = new System.Data.DataTable();
    //            SqlCeConnection conn = new SqlCeConnection(connectionString);
    //            sqlCEEngine = new SqlCeEngine(connectionString);
    //            sqlCEEngine.Upgrade();
    //            conn.Open();

    //            string text = "UPDATE t_setup SET expire_date = '" + _licence_key.Trim() + "', update_date = GETDATE() ";
    //            SqlCeCommand command = conn.CreateCommand();
    //            command.CommandText = text;
    //            command.CommandType = System.Data.CommandType.Text;
    //            if (command.ExecuteNonQuery() > 0)
    //                result = true;

    //            /*
    //            string text = "UPDATE t_setup SET expire_date = '" + _licence_key.Trim() + "', update_date = GETDATE() ";
    //            SqlCeCommand sqlCeCommand = new SqlCeCommand();
    //            sqlCeCommand.CommandType = CommandType.Text;
    //            sqlCeCommand.CommandText = text.ToString();
    //            this.dbManager.Open();
    //            this.dbManager.ExecuteNonQuery(sqlCeCommand);
    //            //this.dbManager.Close();
    //            result = true;
    //            */
    //        }
    //        catch (Exception)
    //        {
    //            result = false;
    //        }

    //        return result;
    //    }
    //    public string ValidationLicence(string _licence_keys)
    //    {
    //        string Result = string.Empty;
    //        try
    //        {
    //            DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
    //            string _licence_key = Convert.ToString((Convert.ToInt64(_licence_keys) - 39335472));
    //            string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);
    //            if (!string.IsNullOrEmpty(_licence_key))
    //            {
    //                if (_licence_key.Trim() == "30000101")
    //                {
    //                    if (ActivateLicence(_licence_keys))
    //                    {
    //                        Result = "TRUE";
    //                    }
    //                }
    //                else if (_licence_key.IndexOf("2016") >= 0)
    //                {
    //                    if (ActivateLicence(_licence_keys))
    //                    {
    //                        DateTime dTime = DateTime.ParseExact(_licence_key, "yyyyMMdd", null);
    //                        string display_date = dTime.ToLongDateString();
    //                        Result = "TRUE_EXPRIE|" + display_date;
    //                    }
    //                }
    //                else if (Convert.ToInt32(_licence_key) < Convert.ToInt32(_crrent_date))
    //                {
    //                    Result = "TRUE_EXPIRED";
    //                }
    //                else
    //                {
    //                    Result = "FALSE";
    //                }
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            Result = "FALSE";
    //        }

    //        return Result;
    //    }

    //    #endregion

    //    #region IDisposable Members

    //    public void Dispose()
    //    {
    //        GC.SuppressFinalize(this);
    //    }

    //    #endregion
    //}
}