﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data;
using System.Data.SqlServerCe;
using System.Data.SqlTypes;
using UI.Admin.CallingService;
using UI.Admin.Execute;

namespace UI.Admin.Connections
{
    public partial class Connection
    {
        #region Property

        protected CallService dbManager;

        #endregion

        #region Method

        public Connection()
        {
            string connectionString = @"Data Source=C:\Data Base SDF\TJG.sdf;Password=pass;Persist Security Info=True";

            dbManager = new CallService(connectionString);
            try
            {
                dbManager.Open();
            }
            catch (SqlCeException sqlex)
            {
                Executing.Instance.Insert_Log("PC_CONNSQLCEException", sqlex.Message.ToString(), "Class_Connection", "Method_Connection()");
            }
            catch (Exception ex)
            {
                Executing.Instance.Insert_Log("PC_CONNException", ex.Message.ToString(), "Class_Connection", "Method_Connection()");
            }
        }

        #endregion
    }
}