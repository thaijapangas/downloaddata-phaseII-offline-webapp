﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UI.Admin.Execute;

namespace UI.Admin
{
    public partial class Mamgements : System.Web.UI.Page
    {
        #region Member

        // Page Customer
        int Cus_pageSize = 15;
        //int totalUsers = 0;
        int Cus_totalPages = 0;
        int Cus_currentPage = 1;

        // Page Loading
        int pageSize = 15;
        //int totalUsers = 0;
        int totalPages = 0;
        int currentPage = 1;

        // Page Items
        int Items_pageSize = 15;
        int Items_totalPages = 0;
        int Items_currentPage = 1;

        //DataTable
        DataTable dtReload;
        DataTable dtReloadItem;
        DataTable dtRead = new DataTable();

        #endregion

        #region Focus TextBox

        void PageSetFocus(Control ctrl)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(ctrl);
        }

        #endregion

        #region Property

        DataTable dtReceive = new DataTable();
        /// <New object RAPI.>
        /// 
        /// </summary>
        private static OpenNETCF.Desktop.Communication.RAPI rapi;

        #endregion

        #region Method

        // Connect Device
        private void ConnectDevice()
        {
            /*
            rapi = new OpenNETCF.Desktop.Communication.RAPI();
            try
            {
                if ((rapi.DevicePresent))
                {
                    //"Connect HandHeld Success";
                    lblResultSync.Text = "Sync HandHeld";
                    lblResultSync.ForeColor = Color.Green;
                }
                else
                {
                    //"Connect HandHeld Not Success";
                    lblResultSync.Text = "Not Sync HandHeld";
                    lblResultSync.ForeColor = Color.Red;
                }
            }
            catch (Exception)
            {
                rapi.Dispose();
            }
            */
        }

        // Paging Loading data.
        private void DoLoading(string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                dtReload = Executing.Instance.getGIDelivery();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                if (dtReload.Rows.Count > 0)
                {
                    dgvGIRefDelivery.DataSource = dtReload;
                    dgvGIRefDelivery.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["do_no"].ToString().Trim();
                                drReload[2] = itemReload["serial_number"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvGIRefDelivery.DataSource = dtReload;
                        dgvGIRefDelivery.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvGIRefDelivery.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("1");
                        return;
                    }

                    dgvGIRefDelivery.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
            else if(_tab.Trim() == "2")
            {
                #region Tab 1.2

                dtReload = Executing.Instance.getDistributionRet();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("empty_or_full");
                dtSubReload.Columns.Add("cust_id");
                dtSubReload.Columns.Add("doc_no");
                dtSubReload.Columns.Add("serial_number");
                dtSubReload.Columns.Add("vehicle");

                if (dtReload.Rows.Count > 0)
                {
                    dgvDistributionRet.DataSource = dtReload;
                    dgvDistributionRet.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["empty_or_full"].ToString().Trim();
                                drReload[2] = itemReload["cust_id"].ToString().Trim();
                                drReload[3] = itemReload["doc_no"].ToString().Trim();
                                drReload[4] = itemReload["serial_number"].ToString().Trim();
                                drReload[5] = itemReload["vehicle"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvDistributionRet.DataSource = dtReload;
                        dgvDistributionRet.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvDistributionRet.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("2");
                        return;
                    }

                    dgvDistributionRet.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
            else if(_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                dtReload = Executing.Instance.getGRFromProduction();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("pre_order");
                dtSubReload.Columns.Add("batch");
                dtSubReload.Columns.Add("serial_number");

                if (dtReload.Rows.Count > 0)
                {
                    dgvGRFromProduction.DataSource = dtReload;
                    dgvGRFromProduction.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["pre_order"].ToString().Trim();
                                drReload[2] = itemReload["batch"].ToString().Trim();
                                drReload[3] = itemReload["serial_number"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvGRFromProduction.DataSource = dtReload;
                        dgvGRFromProduction.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvDistributionRet.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("3");
                        return;
                    }

                    dgvGRFromProduction.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
            else if(_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                dtReload = Executing.Instance.getGIRefIOEmpty();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                if (dtReload.Rows.Count > 0)
                {
                    dgvGIRefIOEmpty.DataSource = dtReload;
                    dgvGIRefIOEmpty.DataBind();

                    //Calculate split page.
                    int counter = 0;
                    int pageIndex = currentPage - 1;
                    int startIndex = pageSize * pageIndex;
                    int endIndex = startIndex + pageSize - 1;

                    if (dtReload.Rows.Count > 0 || dtReload != null)
                    {
                        foreach (DataRow itemReload in dtReload.Rows)
                        {
                            if (counter >= startIndex)
                            {
                                DataRow drReload = dtSubReload.NewRow();
                                drReload[0] = itemReload["create_date"].ToString().Trim();
                                drReload[1] = itemReload["do_no"].ToString().Trim();
                                drReload[2] = itemReload["serial_number"].ToString().Trim();

                                dtSubReload.Rows.Add(drReload);
                            }

                            if (counter >= endIndex) { break; }
                            counter++;
                        }

                        this.lblSearchingCount.Visible = false;
                    }
                    else if (dtReload.Rows.Count == 0 || dtReload == null)
                    {
                        //Not found data.
                        this.lblSearchingCount.Visible = true;
                        this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                        dgvGIRefIOEmpty.DataSource = dtReload;
                        dgvGIRefIOEmpty.DataBind();

                        NextButton.Visible = false;
                        Endpage.Visible = false;
                        PreviousButton.Visible = false;
                        Firstpage.Visible = false;

                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;

                        CurrentPageLabel.Text = "0";
                        TotalPagesLabel.Text = "0";
                        return;
                    }

                    //Control page.
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                    PreviousButton.Visible = true;
                    Firstpage.Visible = true;

                    dgvGIRefIOEmpty.DataSource = dtSubReload;

                    totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                    if (currentPage > totalPages)
                    {
                        currentPage = totalPages;
                        DoLoading("4");
                        return;
                    }

                    dgvGIRefIOEmpty.DataBind();
                    CurrentPageLabel.Text = currentPage.ToString();
                    TotalPagesLabel.Text = totalPages.ToString();

                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.lblpageIndex.Visible = false;
                        this.txtpageIndex.Visible = false;
                        this.btnGO.Visible = false;
                    }
                    else if (this.TotalPagesLabel.Text.Trim() != "1")
                    {
                        this.lblpageIndex.Visible = true;
                        this.txtpageIndex.Visible = true;
                        this.btnGO.Visible = true;
                    }

                    if (currentPage == totalPages)
                    {
                        NextButton.Visible = false;
                        Endpage.Visible = false;
                    }
                    else
                    {
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                    }

                    if (currentPage == 1)
                    {
                        Firstpage.Visible = false;
                        PreviousButton.Visible = false;
                    }
                    else
                    {
                        Firstpage.Visible = true;
                        PreviousButton.Visible = true;
                    }

                    if (dtReload.Rows.Count <= 0)
                        NavigationPanel.Visible = false;
                    else
                        NavigationPanel.Visible = true;
                }

                #endregion
            }
        }
        void DostartPaging(int startPage, string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIDelivery();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefDelivery.DataSource = dtReload;
                    dgvGIRefDelivery.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefDelivery.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "1");
                    return;
                }

                dgvGIRefDelivery.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getDistributionRet();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("empty_or_full");
                dtSubReload.Columns.Add("cust_id");
                dtSubReload.Columns.Add("doc_no");
                dtSubReload.Columns.Add("serial_number");
                dtSubReload.Columns.Add("vehicle");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["empty_or_full"].ToString().Trim();
                            drReload[2] = itemReload["cust_id"].ToString().Trim();
                            drReload[3] = itemReload["doc_no"].ToString().Trim();
                            drReload[4] = itemReload["serial_number"].ToString().Trim();
                            drReload[5] = itemReload["vehicle"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvDistributionRet.DataSource = dtReload;
                    dgvDistributionRet.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvDistributionRet.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "2");
                    return;
                }

                dgvDistributionRet.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGRFromProduction();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("pre_order");
                dtSubReload.Columns.Add("batch");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["pre_order"].ToString().Trim();
                            drReload[2] = itemReload["batch"].ToString().Trim();
                            drReload[3] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGRFromProduction.DataSource = dtReload;
                    dgvGRFromProduction.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGRFromProduction.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "3");
                    return;
                }

                dgvGRFromProduction.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIRefIOEmpty();

                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = startPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefIOEmpty.DataSource = dtReload;
                    dgvGIRefIOEmpty.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefIOEmpty.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (startPage > totalPages)
                {
                    startPage = totalPages;
                    DostartPaging(startPage, "4");
                    return;
                }

                dgvGIRefIOEmpty.DataBind();
                CurrentPageLabel.Text = startPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (startPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (startPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
        }
        void DoendPaging(int lastPage, string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIDelivery();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefDelivery.DataSource = dtReload;
                    dgvGIRefDelivery.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefDelivery.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "1");
                    return;
                }

                dgvGIRefDelivery.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIDelivery();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("empty_or_full");
                dtSubReload.Columns.Add("cust_id");
                dtSubReload.Columns.Add("doc_no");
                dtSubReload.Columns.Add("serial_number");
                dtSubReload.Columns.Add("vehicle");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["empty_or_full"].ToString().Trim();
                            drReload[2] = itemReload["cust_id"].ToString().Trim();
                            drReload[3] = itemReload["doc_no"].ToString().Trim();
                            drReload[4] = itemReload["serial_number"].ToString().Trim();
                            drReload[5] = itemReload["vehicle"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvDistributionRet.DataSource = dtReload;
                    dgvDistributionRet.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvDistributionRet.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "2");
                    return;
                }

                dgvDistributionRet.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGRFromProduction();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("pre_order");
                dtSubReload.Columns.Add("batch");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["pre_order"].ToString().Trim();
                            drReload[2] = itemReload["batch"].ToString().Trim();
                            drReload[3] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGRFromProduction.DataSource = dtReload;
                    dgvGRFromProduction.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGRFromProduction.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "3");
                    return;
                }

                dgvGRFromProduction.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                this.txtpageIndex.Text = string.Empty;
                dtReload = Executing.Instance.getGIRefIOEmpty();
                DataTable dtSubReload = new DataTable();

                //Add colunms
                dtSubReload.Columns.Add("create_date");
                dtSubReload.Columns.Add("do_no");
                dtSubReload.Columns.Add("serial_number");

                //Calculate split page.
                int counter = 0;
                int pageIndex = lastPage - 1;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                if (dtReload.Rows.Count > 0 || dtReload != null)
                {
                    foreach (DataRow itemReload in dtReload.Rows)
                    {
                        if (counter >= startIndex)
                        {
                            DataRow drReload = dtSubReload.NewRow();
                            drReload[0] = itemReload["create_date"].ToString().Trim();
                            drReload[1] = itemReload["do_no"].ToString().Trim();
                            drReload[2] = itemReload["serial_number"].ToString().Trim();

                            dtSubReload.Rows.Add(drReload);
                        }

                        if (counter >= endIndex) { break; }
                        counter++;
                    }

                    this.lblSearchingCount.Visible = false;
                }
                else if (dtReload == null || dtReload.Rows.Count == 0)
                {
                    //Not found data.
                    this.lblSearchingCount.Visible = true;
                    this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                    dgvGIRefIOEmpty.DataSource = dtReload;
                    dgvGIRefIOEmpty.DataBind();

                    NextButton.Visible = false;
                    Endpage.Visible = false;
                    PreviousButton.Visible = false;
                    Firstpage.Visible = false;

                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;

                    CurrentPageLabel.Text = "0";
                    TotalPagesLabel.Text = "0";
                    return;
                }

                //Control page.
                NextButton.Visible = true;
                Endpage.Visible = true;
                PreviousButton.Visible = true;
                Firstpage.Visible = true;

                dgvGIRefIOEmpty.DataSource = dtSubReload;

                totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                if (lastPage > totalPages)
                {
                    lastPage = totalPages;
                    DoendPaging(lastPage, "4");
                    return;
                }

                dgvGIRefIOEmpty.DataBind();
                CurrentPageLabel.Text = lastPage.ToString();
                TotalPagesLabel.Text = totalPages.ToString();

                if (this.TotalPagesLabel.Text.Trim() == "1")
                {
                    this.lblpageIndex.Visible = false;
                    this.txtpageIndex.Visible = false;
                    this.btnGO.Visible = false;
                }
                else if (this.TotalPagesLabel.Text.Trim() != "1")
                {
                    this.lblpageIndex.Visible = true;
                    this.txtpageIndex.Visible = true;
                    this.btnGO.Visible = true;
                }

                if (lastPage == totalPages)
                {
                    NextButton.Visible = false;
                    Endpage.Visible = false;
                }
                else
                {
                    NextButton.Visible = true;
                    Endpage.Visible = true;
                }

                if (lastPage == 1)
                {
                    Firstpage.Visible = false;
                    PreviousButton.Visible = false;
                }
                else
                {
                    Firstpage.Visible = true;
                    PreviousButton.Visible = true;
                }

                if (dtReload.Rows.Count <= 0)
                    NavigationPanel.Visible = false;
                else
                    NavigationPanel.Visible = true;

                #endregion
            }
        }
        void DogotoPage(int index, string _tab)
        {
            if (_tab.Trim() == "1")
            {
                #region Tab 1.1.2

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getGIDelivery();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("do_no");
                        goPage.Columns.Add("serial_number");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["do_no"].ToString().Trim();
                                    drgoPage[2] = itemReload["serial_number"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvGIRefDelivery.DataSource = goPage;
                            dgvGIRefDelivery.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvGIRefDelivery.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "1");
                            return;
                        }

                        dgvGIRefDelivery.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
            else if (_tab.Trim() == "2")
            {
                #region Tab 1.2

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getDistributionRet();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("empty_or_full");
                        goPage.Columns.Add("cust_id");
                        goPage.Columns.Add("doc_no");
                        goPage.Columns.Add("serial_number");
                        goPage.Columns.Add("vehicle");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["empty_or_full"].ToString().Trim();
                                    drgoPage[2] = itemReload["cust_id"].ToString().Trim();
                                    drgoPage[3] = itemReload["doc_no"].ToString().Trim();
                                    drgoPage[4] = itemReload["serial_number"].ToString().Trim();
                                    drgoPage[5] = itemReload["vehicle"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvDistributionRet.DataSource = goPage;
                            dgvDistributionRet.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvDistributionRet.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "2");
                            return;
                        }

                        dgvGIRefDelivery.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
            else if (_tab.Trim() == "3")
            {
                #region Tab 1.4.1

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getGRFromProduction();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("pre_order");
                        goPage.Columns.Add("batch");
                        goPage.Columns.Add("serial_number");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["pre_order"].ToString().Trim();
                                    drgoPage[2] = itemReload["batch"].ToString().Trim();
                                    drgoPage[3] = itemReload["serial_number"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvGRFromProduction.DataSource = goPage;
                            dgvGRFromProduction.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvGRFromProduction.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "3");
                            return;
                        }

                        dgvGRFromProduction.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
            else if (_tab.Trim() == "4")
            {
                #region Tab 1.5.3

                if (index <= 0)
                {
                    if (this.TotalPagesLabel.Text.Trim() == "0")
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Not found data of page", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                    else
                    {
                        //Alert.
                        Page.ScriptJqueryMessage("Input page correct format", JMessageType.Warning);
                        this.txtpageIndex.Text = string.Empty;
                        return;
                    }
                }
                else
                {
                    if (this.TotalPagesLabel.Text.Trim() == "1")
                    {
                        this.txtpageIndex.Attributes.Add("onfocus", "selectText();");
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else if (index > Convert.ToInt32(this.TotalPagesLabel.Text.Trim()))
                    {
                        this.txtpageIndex.Text = string.Empty;
                        PageSetFocus(this.txtpageIndex);
                        return;
                    }
                    else
                    {
                        #region Go to Page

                        dtReload = Executing.Instance.getGIRefIOEmpty();
                        DataTable goPage = new DataTable();

                        //Add colunms
                        goPage.Columns.Add("create_date");
                        goPage.Columns.Add("do_no");
                        goPage.Columns.Add("serial_number");

                        int counter = 0;
                        int pageIndex = index - 1;
                        int startIndex = pageSize * pageIndex;
                        int endIndex = startIndex + pageSize - 1;

                        if (dtReload.Rows.Count > 0 || dtReload != null)
                        {
                            foreach (DataRow itemReload in dtReload.Rows)
                            {
                                if (counter >= startIndex)
                                {
                                    DataRow drgoPage = goPage.NewRow();
                                    drgoPage[0] = itemReload["create_date"].ToString().Trim();
                                    drgoPage[1] = itemReload["do_no"].ToString().Trim();
                                    drgoPage[2] = itemReload["serial_number"].ToString().Trim();

                                    goPage.Rows.Add(drgoPage);
                                }

                                if (counter >= endIndex) { break; }
                                counter++;
                            }

                            this.lblSearchingCount.Visible = false;
                        }
                        else if (dtReload == null || dtReload.Rows.Count == 0)
                        {
                            //Not found data.
                            this.lblSearchingCount.Visible = true;
                            this.lblSearchingCount.Text = string.Format(@"<b>Not fund data</b>&nbsp;&nbsp;&nbsp;{0} &nbsp;row", "0");

                            dgvGIRefIOEmpty.DataSource = goPage;
                            dgvGIRefIOEmpty.DataBind();

                            NextButton.Visible = false;
                            Endpage.Visible = false;
                            PreviousButton.Visible = false;
                            Firstpage.Visible = false;

                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;

                            CurrentPageLabel.Text = "0";
                            TotalPagesLabel.Text = "0";
                            return;
                        }

                        //Control page.
                        NextButton.Visible = true;
                        Endpage.Visible = true;
                        PreviousButton.Visible = true;
                        Firstpage.Visible = true;

                        dgvGIRefIOEmpty.DataSource = goPage;

                        totalPages = ((dtReload.Rows.Count - 1) / pageSize) + 1;

                        if (index > totalPages)
                        {
                            index = totalPages;
                            DogotoPage(index, "4");
                            return;
                        }

                        dgvGIRefIOEmpty.DataBind();
                        CurrentPageLabel.Text = index.ToString();
                        TotalPagesLabel.Text = totalPages.ToString();

                        if (this.TotalPagesLabel.Text.Trim() == "1")
                        {
                            this.lblpageIndex.Visible = false;
                            this.txtpageIndex.Visible = false;
                            this.btnGO.Visible = false;
                        }
                        else if (this.TotalPagesLabel.Text.Trim() != "1")
                        {
                            this.lblpageIndex.Visible = true;
                            this.txtpageIndex.Visible = true;
                            this.btnGO.Visible = true;
                        }

                        if (index == totalPages)
                        {
                            NextButton.Visible = false;
                            Endpage.Visible = false;
                        }
                        else
                        {
                            NextButton.Visible = true;
                            Endpage.Visible = true;
                        }

                        if (index == 1)
                        {
                            Firstpage.Visible = false;
                            PreviousButton.Visible = false;
                        }
                        else
                        {
                            Firstpage.Visible = true;
                            PreviousButton.Visible = true;
                        }

                        if (dtReload.Rows.Count <= 0)
                            NavigationPanel.Visible = false;
                        else
                            NavigationPanel.Visible = true;

                        #endregion
                    }
                }

                #endregion
            }
        }

        private void ExportData(DataTable dtExport, string _tabName)
        {
            switch (_tabName)
            {
                case "1":
                    var grid = new System.Web.UI.WebControls.GridView();
                    grid.DataSource = dtExport;
                    grid.DataBind();

                    /*
                    foreach (GridViewRow oItem in grid.Rows)
                    {
                        oItem.Cells[0].Attributes.Add("class", "td");
                        oItem.Cells[4].Attributes.Add("class", "td");

                        if (!string.IsNullOrEmpty(oItem.Cells[12].Text))
                        {
                            if (Convert.ToDecimal(oItem.Cells[12].Text) < 0)
                            {
                                oItem.Cells[12].Text = "(" + oItem.Cells[12].Text + ")";
                                oItem.Cells[12].Attributes.Add("class", "fontred");
                            }
                        }
                    }
                    */

                    Response.ClearContent();
                    Response.AddHeader("content-disposition", "kattachment; filename=Tab1.xls");
                    Response.ContentType = "application/excel";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);

                    grid.RenderControl(htw);

                    Response.Write(@"<html><body>" + sw.ToString() + "</body></html>");

                    Response.End();
                    break;
            }

        }

        #endregion

        #region Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                if (Session["secLogin"] == null)
                {
                    Response.Redirect("~/Login.aspx");
                }

                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;

                //Action Form Load.
                ConnectDevice();

                this.lblVersion.Text = " Version : " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
        }
        protected void Tab2_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 1;
        }
        protected void Tab3_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Clicked";
            Tab4.CssClass = "Initial";
            MainView.ActiveViewIndex = 2;
        }
        protected void Tab4_Click(object sender, EventArgs e)
        {
            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Initial";
            Tab3.CssClass = "Initial";
            Tab4.CssClass = "Clicked";
            MainView.ActiveViewIndex = 3;
        }

        protected void btnReceiveData_Click(object sender, EventArgs e)
        {
            //Receive Data From.
            dtReceive = null;
            //นำเข้าข้อมูล
            try
            {
                if (!string.IsNullOrEmpty(this.txthidConfirm.Value))
                {
                    if(this.txthidConfirm.Value == "Yes")
                    {
                        #region Confirm

                        // event occures
                        if (lblResultSync.ForeColor == Color.Red)
                        {
                            Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                            return;
                        }
                        else
                        {
                            using(OpenNETCF.Desktop.Communication.RAPI rapi = new OpenNETCF.Desktop.Communication.RAPI())
                            {
                                if (rapi.DevicePresent)
                                {
                                    if (!rapi.Connected)
                                    {
                                        //Create folder Data Base SDF in C:
                                        string _create_path = @"C:\Data Base SDF\";
                                        //Copy files.
                                        string path = @"C:\Data Base SDF\TJG.sdf";
                                        string pathPDA = "\\My Documents\\TJG.sdf";
                                        //Check path in local.
                                        if (!Directory.Exists(_create_path))
                                            Directory.CreateDirectory(_create_path);
                                        //Class copy files from device to PC.
                                        CopyFiles.Files.ClsCopyFile.CopyFromDevice(path, pathPDA);
                                        rapi.Connect();
                                        rapi.Disconnect();
                                    }
                                    //rapi.Dispose();

                                    // Validation expire software.
                                    if (Executing.Instance.CheckExpireSoftware().Trim() == "TRUE")
                                    {
                                        this.btnActivateLicence.Visible = true;
                                    }
                                    else if (Executing.Instance.CheckExpireSoftware().Trim() == "LIFETIME")
                                    {
                                        this.btnActivateLicence.Visible = false;
                                    }
                                    else if (Executing.Instance.CheckExpireSoftware().Trim() == "FALSE")
                                    {
                                        ScriptManager.RegisterStartupScript(
                                                        this,
                                                        GetType(),
                                                        "key",
                                                        "alert('โปรแกรมหมดอายุการใช้งาน กรุณาติดต่อผู้ดูแลระบบเพื่อทำการลงทะเบียนโปรแกรม');",
                                                        true
                                                        );

                                        Response.Redirect("~/ActivateLicense.aspx");
                                    }

                                    //Query data in sqlCe.
                                    if (this.Tab1.CssClass.ToString() == "Clicked")
                                    {
                                        // Select Tab1.
                                        // Data 1.1.2
                                        // Binding data customer.
                                        System.Data.DataTable gIDelivery = Executing.Instance.getGIDelivery();
                                        if (gIDelivery.Rows.Count > 0)
                                        {
                                            DoLoading("1");
                                        }
                                        else
                                        {
                                            // DataBind
                                            this.dgvGIRefDelivery.DataSource = gIDelivery;
                                            this.dgvGIRefDelivery.DataBind();
                                            NavigationPanel.Visible = false;
                                        }

                                        Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                    }
                                    else if (this.Tab2.CssClass.ToString() == "Clicked")
                                    {
                                        // Select Tab2.
                                        // Data 1.2
                                        System.Data.DataTable distributionRet = Executing.Instance.getDistributionRet();
                                        if (distributionRet.Rows.Count > 0)
                                        {
                                            DoLoading("2");
                                        }
                                        else
                                        {
                                            // DataBind
                                            this.dgvDistributionRet.DataSource = distributionRet;
                                            this.dgvDistributionRet.DataBind();
                                            NavigationPanel.Visible = false;
                                        }

                                        Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                    }
                                    else if (this.Tab3.CssClass.ToString() == "Clicked")
                                    {
                                        // Select Tab3.
                                        // Data 1.4.1
                                        System.Data.DataTable gRFromProduction = Executing.Instance.getGRFromProduction();
                                        if (gRFromProduction.Rows.Count > 0)
                                        {
                                            DoLoading("3");
                                        }
                                        else
                                        {
                                            // DataBind
                                            this.dgvGRFromProduction.DataSource = gRFromProduction;
                                            this.dgvGRFromProduction.DataBind();
                                            NavigationPanel.Visible = false;
                                        }

                                        Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                    }
                                    else if (this.Tab4.CssClass.ToString() == "Clicked")
                                    {
                                        // Select Tab4.
                                        // Data 1.5.3
                                        System.Data.DataTable gIRefIOEmpty = Executing.Instance.getGIRefIOEmpty();
                                        if (gIRefIOEmpty.Rows.Count > 0)
                                        {
                                            DoLoading("4");
                                        }
                                        else
                                        {
                                            // DataBind
                                            this.dgvGIRefIOEmpty.DataSource = gIRefIOEmpty;
                                            this.dgvGIRefIOEmpty.DataBind();
                                            NavigationPanel.Visible = false;
                                        }

                                        Page.ScriptJqueryMessage("โหลดข้อมูล สำเร็จ", JMessageType.Accept);
                                    }
                                }
                                else
                                {
                                    Page.ScriptJqueryMessage("HandHeld ไม่ถูกเชื่อมต่อ กรุณาตรวจสอบ", JMessageType.Warning);
                                    return;
                                }
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region Not confirm

                        // do nothing
                        return;

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToString().IndexOf("used by another process") > -1)
                {
                    rapi.Dispose();
                    //this.lblNotify.Text = "";
                    Page.ScriptJqueryMessage("มีการใช้งานฐานข้อมูล" + System.Environment.NewLine + " กรุณาปิดโปรแกรมบน HandHeld หรือ " + System.Environment.NewLine + ex.Message.ToString().Trim(), JMessageType.Warning);
                    Executing.Instance.Insert_Log("PC_LoadData", "used by another process", "Class_CopyFromDevice", "btnReceiveData_Click");

                    // this.btnRefreshConnection.Visible = true;
                }
                else
                {
                    rapi.Dispose();
                    //this.lblNotify.Text = "";
                    Page.ScriptJqueryMessage("โปรแกรมบน HandHeld อาจถูกเปิดอยู่" + System.Environment.NewLine + " กรุณาปิดโปรแกรมบน HandHeld หรือ " + System.Environment.NewLine + "ไม่มีไฟล์ฐานข้อมูล", JMessageType.Warning);
                    Executing.Instance.Insert_Log("PC_LoadData", "used by another process", "Class_CopyFromDevice", "btnReceiveData_Click");

                    //this.btnRefreshConnection.Visible = false;
                }
            }
        }

        protected void Firstpage_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "1");
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "2");
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "3");
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    DostartPaging(1, "4");
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("First page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void PreviousButton_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage--;
                DoLoading("1");
            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage--;
                DoLoading("2");
            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage--;
                DoLoading("3");
            }
            else if (this.Tab4.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage--;
                DoLoading("4");
            }
            
        }
        protected void NextButton_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage++;
                DoLoading("1");
            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage++;
                DoLoading("2");
            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage++;
                DoLoading("3");
            }
            else if (this.Tab4.CssClass.ToString() == "Clicked")
            {
                currentPage = Convert.ToInt32(CurrentPageLabel.Text);
                currentPage++;
                DoLoading("4");
            }
        }
        protected void Endpage_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                    if (lastPage > 0)
                    {
                        DoendPaging(lastPage, "1");
                    }
                    else if (lastPage <= 0)
                    {
                        return;
                    }
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                    if (lastPage > 0)
                    {
                        DoendPaging(lastPage, "2");
                    }
                    else if (lastPage <= 0)
                    {
                        return;
                    }
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                    if (lastPage > 0)
                    {
                        DoendPaging(lastPage, "3");
                    }
                    else if (lastPage <= 0)
                    {
                        return;
                    }
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    int lastPage = Convert.ToInt32(this.TotalPagesLabel.Text.Trim());
                    if (lastPage > 0)
                    {
                        DoendPaging(lastPage, "4");
                    }
                    else if (lastPage <= 0)
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("End page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }
        protected void btnGO_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Tab1.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "1");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
                else if (this.Tab2.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "2");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
                else if (this.Tab3.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "3");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
                else if (this.Tab4.CssClass.ToString() == "Clicked")
                {
                    if (!string.IsNullOrEmpty(this.txtpageIndex.Text.Trim()))
                        DogotoPage(Convert.ToInt32(this.txtpageIndex.Text.Trim()), "4");
                    else
                        PageSetFocus(this.txtpageIndex);
                }
            }
            catch (Exception ex)
            {
                Page.ScriptJqueryMessage("Go to page : " + ex.Message.ToString(), JMessageType.Error);
            }
        }

        // Button Export
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Tab1.CssClass.ToString() == "Clicked")
            {
                // Select Tab1.
                DataTable dtTab1 = new DataTable("Tab1");

                //Add columns to DataTable.
                foreach (TableCell cell in dgvGIRefDelivery.HeaderRow.Cells)
                {
                    dtTab1.Columns.Add(cell.Text);
                }

                //Loop through the GridView and copy rows.
                foreach (GridViewRow row in dgvGIRefDelivery.Rows)
                {
                    dtTab1.Rows.Add();
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        dtTab1.Rows[row.RowIndex][i] = row.Cells[i].Text;
                    }
                }
                ExportData(dtTab1, "1");
            }
            else if (this.Tab2.CssClass.ToString() == "Clicked")
            {
                // Select Tab2.

            }
            else if (this.Tab3.CssClass.ToString() == "Clicked")
            {
                // Select Tab3.

            }
        }
        protected void btnActivateLicence_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ActivateLicense.aspx");
        }

        protected void UpdateTimer_Tick(object sender, EventArgs e)
        {
            ConnectDevice();
        }

        #endregion
    }
}